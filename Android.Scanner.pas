unit Android.Scanner;

interface

uses  Android.JNI.Ingenico, Androidapi.JNI.Toast, Android.BroadcastReceiver, Androidapi.IOUtils, Androidapi.JNI.Os,
  Androidapi.JNI.GraphicsContentViewText, Androidapi.JNIBridge, Androidapi.Helpers, Androidapi.JNI, Androidapi.JNI.JavaTypes,
  FMX.Helpers.Android, FMX.Platform.Android,
  System.Classes, ISMPScanner;

type

  TTestConn2 = Class(TJavaLocal, JServiceConnection)
  private
    FContext: JContext;
    FService: JIPclService;
    FServiceConn: JServiceConnection;
    FServiceAlive: Boolean;
    ServiceIntent: JIntent;

    FIntentFilter: JIntentFilter;
    // the receiver
    BroadcastReceiver1: TBroadcastReceiver2;
    FBarcodeNotify: TGetStrProc;
    procedure StartService;
    procedure StopService;
    function getservice: JIPclService;
    function GetServiceAlive: Boolean;
    // function InitReceiver: Boolean;
    procedure SetServiceAlive(const Value: Boolean);
  public
    constructor Create;
    destructor Destroy; Override;
    procedure onServiceConnected(name: JComponentName; service: JIBinder); cdecl;
    procedure onServiceDisconnected(name: JComponentName); cdecl;
    procedure BroadcastReceiver1Receive(Context: JContext; Intent: JIntent);

    property service: JIPclService read getservice;
    property ServiceAlive: Boolean Read GetServiceAlive write SetServiceAlive;
    property BarcodeCallback: TGetStrProc read FBarcodeNotify write FBarcodeNotify;
  End;

  TAndroidScan = class(TISMPScan)
  private
    testaConn: TTestConn2;
    function getactive: Boolean; override;
    procedure setActive(const Value: Boolean); Override;

  public
      procedure Init; Override;
      function BatteryLevel: integer; override;
      function ScannerOpen: Boolean; Override;
      function Scan: Boolean; Override;
      procedure ScannerClose; Override;
      procedure Close; Override;

      property OnScan: TGetStrProc read FOnScan write FOnScan;
      property Active: Boolean read getactive write setActive;
  end;


implementation

uses System.sysUtils;

{ TTestConn2 }

procedure TTestConn2.BroadcastReceiver1Receive(Context: JContext; Intent: JIntent);
var
  // telephonyManager: JTelephonyManager;
  // obj: JObject;
  // Temp: String;
  temp: TJavaArray<byte>;
  tmp: string;
  i: integer;
begin
  // if not BroadcastReceiver1.HasPermission('android.permission.READ_PHONE_STATE') then
  // begin
  // ShowMessage('You don''t have permission for Read Phone State!');
  // Exit;
  // end;

  // Obj := SharedActivityContext.getSystemService(TJContext.JavaClass.TELEPHONY_SERVICE);
  // telephonyManager := TJTelephonyManager.Wrap( (obj as ILocalObject).GetObjectID );
  tmp := '';
  temp := Intent.getByteArrayExtra(StringToJString('barcode'));
  if temp.Length > 0 then
  Begin
    tmp := '';
    for i := 0 to temp.Length do
      tmp := tmp + char(temp[i]);

    // Toast('Barcode: ' + tmp, TToastLength.LongToast);
  end;

  if Assigned(FBarcodeNotify) then
    FBarcodeNotify(tmp);
end;

constructor TTestConn2.Create;
begin
  inherited Create;
  FContext := SharedActivityContext.getApplicationContext;
end;

function TTestConn2.GetServiceAlive: Boolean;
begin
  Result := (FServiceConn <> nil) and (FService <> nil) and FServiceAlive;
end;

destructor TTestConn2.Destroy;
begin
  BroadcastReceiver1.Free;
  inherited;
end;

function TTestConn2.getservice: JIPclService;
begin
  Result := FService
end;

procedure TTestConn2.onServiceConnected(name: JComponentName; service: JIBinder);
var
  j: JObject;
begin
  FService := TJIPclService_Stub.JavaClass.asInterface(service);
  if FService <> nil then
  Begin
    ServiceAlive := True;
    Toast('Service Running', TToastLength.ShortToast);
  End;
end;

procedure TTestConn2.onServiceDisconnected(name: JComponentName);
begin
  FContext.unbindService(FServiceConn);
  FServiceConn := nil;
  FService := nil;
  ServiceAlive := False;
  Toast('Service Stopped', TToastLength.ShortToast);
end;

procedure TTestConn2.SetServiceAlive(const Value: Boolean);
begin
  if Value then
  begin
    if BroadcastReceiver1 = nil then
    begin
      BroadcastReceiver1 := TBroadcastReceiver2.Create;
      BroadcastReceiver1.onReceive := BroadcastReceiver1Receive;
    end;
    BroadcastReceiver1.RegisterReceive;
    BroadcastReceiver1.Add('com.ingenico.pclservice.action.BARCODE_EVENT');
    BroadcastReceiver1.Add('com.ingenico.pclservice.action.BARCODE_Closed');
  end
  else
  begin
    FreeandNil(BroadcastReceiver1);
  end;
  FServiceAlive := Value;
end;

procedure TTestConn2.StartService;
var
  MyName: JComponentName;
begin

  ServiceIntent := TJIntent.Create;
  ServiceIntent.setClassName(StringToJString('com.ingenico.pclservice'), StringToJString('com.ingenico.pclservice.PclService'));

  FServiceConn := TJServiceConnection.Wrap((Self as ILocalObject).GetObjectID);

  FContext.bindService(ServiceIntent, FServiceConn, TJContext.JavaClass.BIND_AUTO_CREATE);

  MyName := FContext.StartService(ServiceIntent);
end;

procedure TTestConn2.StopService;
begin
  if FServiceAlive and (FServiceConn <> nil) then
  begin
    if FContext <> nil then
    begin
      FContext.unbindService(FServiceConn); // Roy: this disconnect from the service but does not seem to call on onservicedisconnect
      FContext.StopService(ServiceIntent);
    end;
    FServiceConn := nil;
    FService := nil;
    FServiceAlive := False;
  end;
end;

{ TAndroidScan }

function TAndroidScan.BatteryLevel: integer;
begin

end;

procedure TAndroidScan.Close;
begin
  testaConn.StopService;
end;

function TAndroidScan.getactive: Boolean;
begin
  Result := testaConn.ServiceAlive;
end;

procedure TAndroidScan.Init;
begin
  if not Assigned(testaConn) then
  begin
    testaConn := TTestConn2.Create;
    testaConn.BarcodeCallback := FOnScan;
  end;
  testaConn.StartService;
end;

function TAndroidScan.Scan: Boolean;
begin

end;

procedure TAndroidScan.ScannerClose;
begin

end;

function TAndroidScan.ScannerOpen: Boolean;
var
  ja : TJavaArray<byte>;
begin
  if testaConn.ServiceAlive then
  begin
    ja := TJavaArray<byte>.Create(1);
    try
      ja[0] := 0;
      Result := testaConn.service.openBarcode(ja);

      if not Result and (ja[0] = 1) then
        testaConn.service.closeBarcode(ja);

    finally
      ja.Free
    end;
  end
end;

procedure TAndroidScan.setActive(const Value: Boolean);
begin
  if value and not testaConn.ServiceAlive then
    testaconn.StartService
  else
  if not value and testaConn.ServiceAlive then
    testaconn.StopService;
end;

end.
