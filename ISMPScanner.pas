unit ISMPScanner;

interface

uses System.Classes;

type
  TISMPScan = class(Tobject)
  protected
    FOnScan: TGetStrProc;

    function getactive: Boolean; virtual;
    procedure setActive(const Value: Boolean); virtual; abstract;
  public
    procedure Init; virtual; abstract;
    function BatteryLevel: integer; virtual;
    function ScannerOpen: Boolean; virtual;
    function Scan: Boolean; virtual;
    procedure ScannerClose; virtual; abstract;
    procedure Close; virtual; abstract;

    property OnScan: TGetStrProc read FOnScan write FOnScan;
    property Active: Boolean read getactive write setActive;
  end;


implementation

{ TISMPScan }

function TISMPScan.BatteryLevel: integer;
begin
  Result := -1
end;

function TISMPScan.getactive: Boolean;
begin
  Result := False;
end;

function TISMPScan.Scan: Boolean;
begin
  Result := False;
end;

function TISMPScan.ScannerOpen: Boolean;
begin
  Result := False;
end;

end.
