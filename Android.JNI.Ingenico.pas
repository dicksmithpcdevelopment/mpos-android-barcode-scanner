{*******************************************************}
{                                                       }
{           CodeGear Delphi Runtime Library             }
{ Copyright(c) 2014 Embarcadero Technologies, Inc.      }
{                                                       }
{*******************************************************}

unit Android.JNI.Ingenico;

interface

uses
  Androidapi.JNIBridge,
  Androidapi.JNI.JavaTypes,
  Androidapi.JNI.Widget,
  Androidapi.JNI.GraphicsContentViewText,
  Androidapi.JNI.Util,
  Androidapi.JNI.Os,
  Androidapi.JNI.Net,
  Androidapi.JNI.App;

type
// ===== Forward declarations =====

  JBitmapConvertor = interface;//com.ingenico.pclservice.BitmapConvertor
  JBluetoothService = interface;//com.ingenico.pclservice.BluetoothService
  JBluetoothService_1 = interface;//com.ingenico.pclservice.BluetoothService$1
  JBluetoothService_2 = interface;//com.ingenico.pclservice.BluetoothService$2
  JBluetoothService_BluetoothThread = interface;//com.ingenico.pclservice.BluetoothService$BluetoothThread
  JBluetoothService_IpThread = interface;//com.ingenico.pclservice.BluetoothService$IpThread
  JBluetoothService_IpTxThread = interface;//com.ingenico.pclservice.BluetoothService$IpTxThread
  JBluetoothService_Status = interface;//com.ingenico.pclservice.BluetoothService$Status
  JIPclService = interface;//com.ingenico.pclservice.IPclService
  JIPclService_Stub = interface;//com.ingenico.pclservice.IPclService$Stub
  JIPclService_Stub_Proxy = interface;//com.ingenico.pclservice.IPclService$Stub$Proxy
  JIPclServiceCallback = interface;//com.ingenico.pclservice.IPclServiceCallback
  JIPclServiceCallback_Stub = interface;//com.ingenico.pclservice.IPclServiceCallback$Stub
  JIPclServiceCallback_Stub_Proxy = interface;//com.ingenico.pclservice.IPclServiceCallback$Stub$Proxy
  JNetStat = interface;//com.ingenico.pclservice.NetStat
  JNetStat_Connection = interface;//com.ingenico.pclservice.NetStat$Connection
  JPclService = interface;//com.ingenico.pclservice.PclService
  JPclService_1 = interface;//com.ingenico.pclservice.PclService$1
  JPclService_2 = interface;//com.ingenico.pclservice.PclService$2
  JTimerTask = interface;//java.util.TimerTask
  JPclService_3 = interface;//com.ingenico.pclservice.PclService$3
  JPclService_LocalBinder = interface;//com.ingenico.pclservice.PclService$LocalBinder
  JPclService_SignObject = interface;//com.ingenico.pclservice.PclService$SignObject
  JPclService_Symbologies = interface;//com.ingenico.pclservice.PclService$Symbologies
  JPclService_TextObject = interface;//com.ingenico.pclservice.PclService$TextObject
  JTransactionIn = interface;//com.ingenico.pclservice.TransactionIn
  JTransactionIn_1 = interface;//com.ingenico.pclservice.TransactionIn$1
  JTransactionIn_AuthorizationType = interface;//com.ingenico.pclservice.TransactionIn$AuthorizationType
  JTransactionIn_CheckControl = interface;//com.ingenico.pclservice.TransactionIn$CheckControl
  JTransactionIn_OperationType = interface;//com.ingenico.pclservice.TransactionIn$OperationType
  JTransactionOut = interface;//com.ingenico.pclservice.TransactionOut
  JTransactionOut_1 = interface;//com.ingenico.pclservice.TransactionOut$1
  JTransactionOut_ErrorCode = interface;//com.ingenico.pclservice.TransactionOut$ErrorCode

// ===== Interface declarations =====

  JBitmapConvertorClass = interface(JObjectClass)
    ['{899ED118-C4FE-43E3-BED7-55726D16F9C5}']
    {class} function init: JBitmapConvertor; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/BitmapConvertor')]
  JBitmapConvertor = interface(JObject)
    ['{BB971F48-DC33-49E8-9529-9840F020AA44}']
    function convertBitmap(P1: JBitmap): JByteBuffer; cdecl;
  end;
  TJBitmapConvertor = class(TJavaGenericImport<JBitmapConvertorClass, JBitmapConvertor>) end;

  JBluetoothServiceClass = interface(JServiceClass)
    ['{3275C0E2-1D5C-496D-BE88-6622C172C048}']
    {class} function _GetRCVPORT: Integer;
    {class} function init: JBluetoothService; cdecl;
    {class} property RCVPORT: Integer read _GetRCVPORT;
  end;

  [JavaSignature('com/ingenico/pclservice/BluetoothService')]
  JBluetoothService = interface(JService)
    ['{019CE120-6FA0-4EA4-BC63-6A1BE7247E31}']
    function onBind(P1: JIntent): JIBinder; cdecl;
    procedure onCreate; cdecl;
    procedure onDestroy; cdecl;
    function onStartCommand(P1: JIntent; P2: Integer; P3: Integer): Integer; cdecl;
  end;
  TJBluetoothService = class(TJavaGenericImport<JBluetoothServiceClass, JBluetoothService>) end;

  JBluetoothService_1Class = interface(JBroadcastReceiverClass)
    ['{F128AE8C-B2C5-4216-B9ED-B855259A8982}']
  end;

  [JavaSignature('com/ingenico/pclservice/BluetoothService$1')]
  JBluetoothService_1 = interface(JBroadcastReceiver)
    ['{4796B0B0-19F6-4B39-A14C-81C7112E49CB}']
    procedure onReceive(P1: JContext; P2: JIntent); cdecl;
  end;
  TJBluetoothService_1 = class(TJavaGenericImport<JBluetoothService_1Class, JBluetoothService_1>) end;

  JBluetoothService_2Class = interface(JHandlerClass)
    ['{A945DB50-8958-4E5B-B0DF-F46E5723BFF5}']
  end;

  [JavaSignature('com/ingenico/pclservice/BluetoothService$2')]
  JBluetoothService_2 = interface(JHandler)
    ['{EA542EEB-3B8B-4499-9BDF-5E227F06E046}']
    procedure handleMessage(P1: JMessage); cdecl;
  end;
  TJBluetoothService_2 = class(TJavaGenericImport<JBluetoothService_2Class, JBluetoothService_2>) end;

  JBluetoothService_BluetoothThreadClass = interface(JThreadClass)
    ['{C4B1B26C-6ECB-4624-A8C7-C164B46E194E}']
  end;

  [JavaSignature('com/ingenico/pclservice/BluetoothService$BluetoothThread')]
  JBluetoothService_BluetoothThread = interface(JThread)
    ['{28712FFD-1560-42F2-B38B-F5F7E5FEEAFF}']
    procedure closeInStream; cdecl;
    procedure run; cdecl;
    procedure write(P1: TJavaArray<Byte>; P2: Integer); cdecl;
  end;
  TJBluetoothService_BluetoothThread = class(TJavaGenericImport<JBluetoothService_BluetoothThreadClass, JBluetoothService_BluetoothThread>) end;

  JBluetoothService_IpThreadClass = interface(JThreadClass)
    ['{A2DE1B68-E9D6-4DB5-96C9-812EEF3DFBD1}']
    {class} function init(P1: JBluetoothService): JBluetoothService_IpThread; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/BluetoothService$IpThread')]
  JBluetoothService_IpThread = interface(JThread)
    ['{94EA0732-9FA2-4259-8435-597E693285CA}']
    procedure cancel; cdecl;
    procedure run; cdecl;
    procedure write(P1: TJavaArray<Byte>; P2: Integer); cdecl;
  end;
  TJBluetoothService_IpThread = class(TJavaGenericImport<JBluetoothService_IpThreadClass, JBluetoothService_IpThread>) end;

  JBluetoothService_IpTxThreadClass = interface(JThreadClass)
    ['{56D81861-3577-4899-B737-77057AB62070}']
    {class} function init(P1: JBlockingQueue): JBluetoothService_IpTxThread; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/BluetoothService$IpTxThread')]
  JBluetoothService_IpTxThread = interface(JThread)
    ['{1206FFFF-191F-469B-8377-3349CA2513D4}']
    procedure cancel; cdecl;
    procedure run; cdecl;
  end;
  TJBluetoothService_IpTxThread = class(TJavaGenericImport<JBluetoothService_IpTxThreadClass, JBluetoothService_IpTxThread>) end;

  JBluetoothService_StatusClass = interface(JEnumClass)
    ['{8AAE3307-F6FC-4C27-9CB1-24261CC556BC}']
    {class} function _GetBLUETOOTH_CONNECTED: JBluetoothService_Status;
    {class} function _GetBLUETOOTH_CONNECTION_FAILED: JBluetoothService_Status;
    {class} function _GetBLUETOOTH_DISCONNECTED: JBluetoothService_Status;
    {class} function _GetNO_BLUETOOTH_ADAPTER: JBluetoothService_Status;
    {class} function _GetNO_BLUETOOTH_PAIRED_DEVICE: JBluetoothService_Status;
    {class} function valueOf(P1: JString): JBluetoothService_Status; cdecl;
    {class} function values: TJavaObjectArray<JBluetoothService_Status>; cdecl;
    {class} property BLUETOOTH_CONNECTED: JBluetoothService_Status read _GetBLUETOOTH_CONNECTED;
    {class} property BLUETOOTH_CONNECTION_FAILED: JBluetoothService_Status read _GetBLUETOOTH_CONNECTION_FAILED;
    {class} property BLUETOOTH_DISCONNECTED: JBluetoothService_Status read _GetBLUETOOTH_DISCONNECTED;
    {class} property NO_BLUETOOTH_ADAPTER: JBluetoothService_Status read _GetNO_BLUETOOTH_ADAPTER;
    {class} property NO_BLUETOOTH_PAIRED_DEVICE: JBluetoothService_Status read _GetNO_BLUETOOTH_PAIRED_DEVICE;
  end;

  [JavaSignature('com/ingenico/pclservice/BluetoothService$Status')]
  JBluetoothService_Status = interface(JEnum)
    ['{0A4638F7-A388-4C13-AF98-194EEA72D108}']
  end;
  TJBluetoothService_Status = class(TJavaGenericImport<JBluetoothService_StatusClass, JBluetoothService_Status>) end;

  JIPclServiceClass = interface(JIInterfaceClass)
    ['{F713203A-DEA9-4FC9-9543-70A40946DEB0}']
  end;

  [JavaSignature('com/ingenico/pclservice/IPclService')]
  JIPclService = interface(JIInterface)
    ['{89F021CA-7B08-4E19-83E7-6F8C36FC0E83}']
    function addDynamicBridge(P1: Integer; P2: Integer): Integer; cdecl;
    function addDynamicBridgeLocal(P1: Integer; P2: Integer): Integer; cdecl;
    function bcrDisableSymbologies(P1: TJavaArray<Integer>; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function bcrEnableSymbologies(P1: TJavaArray<Integer>; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function bcrEnableTrigger(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrGetFirmwareVersion(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrGetSettingsVersion(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetBeep(P1: Integer; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetGoodScanBeep(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetImagerMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetLightingMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetNonVolatileMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetReaderMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetSettingsVersion(P1: TJavaArray<Byte>; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSoftReset(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrStartScan(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrStopScan(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSymbologyToText(P1: Integer): JString; cdecl;
    function closeBarcode(P1: TJavaArray<Byte>): Boolean; cdecl;
    function closePrinter(P1: TJavaArray<Byte>): Boolean; cdecl;
    function doTransaction(P1: JTransactionIn; P2: JTransactionOut): Boolean; cdecl;
    function doTransactionEx(P1: JTransactionIn; P2: JTransactionOut; P3: Integer; P4: TJavaArray<Byte>; P5: Int64; P6: TJavaArray<Byte>; P7: TJavaArray<Int64>): Boolean; cdecl;
    function doUpdate(P1: TJavaArray<Byte>): Boolean; cdecl;
    function flushMessages: Boolean; cdecl;
    function getFullSerialNumber(P1: TJavaArray<Byte>): Boolean; cdecl;
    function getPrinterStatus(P1: TJavaArray<Byte>): Boolean; cdecl;
    function getSPMCIVersion(P1: TJavaArray<Byte>): Boolean; cdecl;
    function getTerminalComponents(P1: JString): Boolean; cdecl;
    function getTerminalInfo(P1: TJavaArray<Byte>; P2: TJavaArray<Byte>): Boolean; cdecl;
    function getTerminalTime(P1: TJavaArray<Byte>): Boolean; cdecl;
    function inputSimul(P1: TJavaArray<Byte>): Boolean; cdecl;
    function isPrinterBatteryLow(P1: TJavaArray<Byte>): Boolean; cdecl;
    function isPrinterConnected(P1: TJavaArray<Byte>): Boolean; cdecl;
    function launchM2OSShortcut(P1: TJavaArray<Byte>): Boolean; cdecl;
    function openBarcode(P1: TJavaArray<Byte>): Boolean; cdecl;
    function openBarcodeWithInactivityTo(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function openPrinter(P1: TJavaArray<Byte>): Boolean; cdecl;
    function printBitmap(P1: TJavaArray<Byte>; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function printBitmapObject(P1: JBitmap; P2: TJavaArray<Byte>): Boolean; cdecl;
    function printLogo(P1: JString; P2: TJavaArray<Byte>): Boolean; cdecl;
    function printText(P1: JString; P2: TJavaArray<Byte>): Boolean; cdecl;
    function receiveMessage(P1: TJavaArray<Byte>; P2: TJavaArray<Integer>): Boolean; cdecl;
    procedure registerCallback(P1: JIPclServiceCallback; P2: Boolean); cdecl;
    function resetTerminal(P1: Integer): Boolean; cdecl;
    function sendMessage(P1: TJavaArray<Byte>; P2: TJavaArray<Integer>): Boolean; cdecl;
    function serverStatus(P1: TJavaArray<Byte>): Boolean; cdecl;
    procedure setSignatureCaptureResult(P1: Integer); cdecl;
    function setTerminalTime(P1: TJavaArray<Byte>): Boolean; cdecl;
    function storeLogo(P1: JString; P2: Integer; P3: TJavaArray<Byte>; P4: Integer; P5: TJavaArray<Byte>): Boolean; cdecl;
    procedure submitSignatureWithImage(P1: JBitmap); cdecl;
    function tmsReadParam(P1: TJavaObjectArray<JString>; P2: TJavaObjectArray<JString>; P3: TJavaObjectArray<JString>; P4: TJavaObjectArray<JString>; P5: TJavaObjectArray<JString>; P6: TJavaArray<Byte>): Boolean; cdecl;
    function tmsWriteParam(P1: JString; P2: JString; P3: JString; P4: JString; P5: TJavaArray<Byte>): Boolean; cdecl;
    procedure unregisterCallback(P1: JIPclServiceCallback); cdecl;
  end;
  TJIPclService = class(TJavaGenericImport<JIPclServiceClass, JIPclService>) end;

  JIPclService_StubClass = interface(JBinderClass)
    ['{81850D49-A31C-4571-B9C0-B701CD5CFE1D}']
    {class} function asInterface(P1: JIBinder): JIPclService; cdecl;
    {class} function init: JIPclService_Stub; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/IPclService$Stub')]
  JIPclService_Stub = interface(JBinder)
    ['{98E761E3-BCAC-4CA7-A0A5-3232CB73F55F}']
    function asBinder: JIBinder; cdecl;
    function onTransact(P1: Integer; P2: JParcel; P3: JParcel; P4: Integer): Boolean; cdecl;
  end;
  TJIPclService_Stub = class(TJavaGenericImport<JIPclService_StubClass, JIPclService_Stub>) end;

  JIPclService_Stub_ProxyClass = interface(JObjectClass)
    ['{A4EE65AC-5667-4593-A530-14303EBF9B28}']
  end;

  [JavaSignature('com/ingenico/pclservice/IPclService$Stub$Proxy')]
  JIPclService_Stub_Proxy = interface(JObject)
    ['{D7C9DBDA-4282-4AD5-B675-C2836B45E58A}']
    function addDynamicBridge(P1: Integer; P2: Integer): Integer; cdecl;
    function addDynamicBridgeLocal(P1: Integer; P2: Integer): Integer; cdecl;
    function asBinder: JIBinder; cdecl;
    function bcrDisableSymbologies(P1: TJavaArray<Integer>; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function bcrEnableSymbologies(P1: TJavaArray<Integer>; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function bcrEnableTrigger(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrGetFirmwareVersion(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrGetSettingsVersion(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetBeep(P1: Integer; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetGoodScanBeep(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetImagerMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetLightingMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetNonVolatileMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetReaderMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetSettingsVersion(P1: TJavaArray<Byte>; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSoftReset(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrStartScan(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrStopScan(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSymbologyToText(P1: Integer): JString; cdecl;
    function closeBarcode(P1: TJavaArray<Byte>): Boolean; cdecl;
    function closePrinter(P1: TJavaArray<Byte>): Boolean; cdecl;
    function doTransaction(P1: JTransactionIn; P2: JTransactionOut): Boolean; cdecl;
    function doTransactionEx(P1: JTransactionIn; P2: JTransactionOut; P3: Integer; P4: TJavaArray<Byte>; P5: Int64; P6: TJavaArray<Byte>; P7: TJavaArray<Int64>): Boolean; cdecl;
    function doUpdate(P1: TJavaArray<Byte>): Boolean; cdecl;
    function flushMessages: Boolean; cdecl;
    function getFullSerialNumber(P1: TJavaArray<Byte>): Boolean; cdecl;
    function getInterfaceDescriptor: JString; cdecl;
    function getPrinterStatus(P1: TJavaArray<Byte>): Boolean; cdecl;
    function getSPMCIVersion(P1: TJavaArray<Byte>): Boolean; cdecl;
    function getTerminalComponents(P1: JString): Boolean; cdecl;
    function getTerminalInfo(P1: TJavaArray<Byte>; P2: TJavaArray<Byte>): Boolean; cdecl;
    function getTerminalTime(P1: TJavaArray<Byte>): Boolean; cdecl;
    function inputSimul(P1: TJavaArray<Byte>): Boolean; cdecl;
    function isPrinterBatteryLow(P1: TJavaArray<Byte>): Boolean; cdecl;
    function isPrinterConnected(P1: TJavaArray<Byte>): Boolean; cdecl;
    function launchM2OSShortcut(P1: TJavaArray<Byte>): Boolean; cdecl;
    function openBarcode(P1: TJavaArray<Byte>): Boolean; cdecl;
    function openBarcodeWithInactivityTo(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function openPrinter(P1: TJavaArray<Byte>): Boolean; cdecl;
    function printBitmap(P1: TJavaArray<Byte>; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function printBitmapObject(P1: JBitmap; P2: TJavaArray<Byte>): Boolean; cdecl;
    function printLogo(P1: JString; P2: TJavaArray<Byte>): Boolean; cdecl;
    function printText(P1: JString; P2: TJavaArray<Byte>): Boolean; cdecl;
    function receiveMessage(P1: TJavaArray<Byte>; P2: TJavaArray<Integer>): Boolean; cdecl;
    procedure registerCallback(P1: JIPclServiceCallback; P2: Boolean); cdecl;
    function resetTerminal(P1: Integer): Boolean; cdecl;
    function sendMessage(P1: TJavaArray<Byte>; P2: TJavaArray<Integer>): Boolean; cdecl;
    function serverStatus(P1: TJavaArray<Byte>): Boolean; cdecl;
    procedure setSignatureCaptureResult(P1: Integer); cdecl;
    function setTerminalTime(P1: TJavaArray<Byte>): Boolean; cdecl;
    function storeLogo(P1: JString; P2: Integer; P3: TJavaArray<Byte>; P4: Integer; P5: TJavaArray<Byte>): Boolean; cdecl;
    procedure submitSignatureWithImage(P1: JBitmap); cdecl;
    function tmsReadParam(P1: TJavaObjectArray<JString>; P2: TJavaObjectArray<JString>; P3: TJavaObjectArray<JString>; P4: TJavaObjectArray<JString>; P5: TJavaObjectArray<JString>; P6: TJavaArray<Byte>): Boolean; cdecl;
    function tmsWriteParam(P1: JString; P2: JString; P3: JString; P4: JString; P5: TJavaArray<Byte>): Boolean; cdecl;
    procedure unregisterCallback(P1: JIPclServiceCallback); cdecl;
  end;
  TJIPclService_Stub_Proxy = class(TJavaGenericImport<JIPclService_Stub_ProxyClass, JIPclService_Stub_Proxy>) end;

  JIPclServiceCallbackClass = interface(JIInterfaceClass)
    ['{830D8834-13CC-444C-BA6C-2E32E1CD76EF}']
  end;

  [JavaSignature('com/ingenico/pclservice/IPclServiceCallback')]
  JIPclServiceCallback = interface(JIInterface)
    ['{ACA74B14-1161-4EF0-AA8B-4354514F04E8}']
    procedure shouldAddSignature; cdecl;
    procedure shouldCutPaper; cdecl;
    procedure shouldDoSignatureCapture(P1: Integer; P2: Integer; P3: Integer; P4: Integer; P5: Integer); cdecl;
    function shouldEndReceipt: Integer; cdecl;
    procedure shouldFeedPaper(P1: Integer); cdecl;
    procedure shouldPrintImage(P1: JBitmap; P2: Byte); cdecl;
    procedure shouldPrintRawText(P1: TJavaArray<Byte>; P2: Byte; P3: Byte; P4: Byte; P5: Byte; P6: Byte; P7: Byte; P8: Byte); cdecl;
    procedure shouldPrintText(P1: JString; P2: Byte; P3: Byte; P4: Byte; P5: Byte; P6: Byte; P7: Byte); cdecl;
    function shouldStartReceipt(P1: Byte): Integer; cdecl;
    procedure signatureTimeoutExceeded; cdecl;
  end;
  TJIPclServiceCallback = class(TJavaGenericImport<JIPclServiceCallbackClass, JIPclServiceCallback>) end;

  JIPclServiceCallback_StubClass = interface(JBinderClass)
    ['{00CD568F-E97A-4A64-80C1-2245B1FDAD42}']
    {class} function asInterface(P1: JIBinder): JIPclServiceCallback; cdecl;
    {class} function init: JIPclServiceCallback_Stub; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/IPclServiceCallback$Stub')]
  JIPclServiceCallback_Stub = interface(JBinder)
    ['{C92ACCD9-174A-48BF-A549-7EE99E69BFEC}']
    function asBinder: JIBinder; cdecl;
    function onTransact(P1: Integer; P2: JParcel; P3: JParcel; P4: Integer): Boolean; cdecl;
  end;
  TJIPclServiceCallback_Stub = class(TJavaGenericImport<JIPclServiceCallback_StubClass, JIPclServiceCallback_Stub>) end;

  JIPclServiceCallback_Stub_ProxyClass = interface(JObjectClass)
    ['{643EA23B-44E0-478E-AA09-954D12FA16CE}']
  end;

  [JavaSignature('com/ingenico/pclservice/IPclServiceCallback$Stub$Proxy')]
  JIPclServiceCallback_Stub_Proxy = interface(JObject)
    ['{321A76BE-8AB3-4F2B-8656-467973814EFD}']
    function asBinder: JIBinder; cdecl;
    function getInterfaceDescriptor: JString; cdecl;
    procedure shouldAddSignature; cdecl;
    procedure shouldCutPaper; cdecl;
    procedure shouldDoSignatureCapture(P1: Integer; P2: Integer; P3: Integer; P4: Integer; P5: Integer); cdecl;
    function shouldEndReceipt: Integer; cdecl;
    procedure shouldFeedPaper(P1: Integer); cdecl;
    procedure shouldPrintImage(P1: JBitmap; P2: Byte); cdecl;
    procedure shouldPrintRawText(P1: TJavaArray<Byte>; P2: Byte; P3: Byte; P4: Byte; P5: Byte; P6: Byte; P7: Byte; P8: Byte); cdecl;
    procedure shouldPrintText(P1: JString; P2: Byte; P3: Byte; P4: Byte; P5: Byte; P6: Byte; P7: Byte); cdecl;
    function shouldStartReceipt(P1: Byte): Integer; cdecl;
    procedure signatureTimeoutExceeded; cdecl;
  end;
  TJIPclServiceCallback_Stub_Proxy = class(TJavaGenericImport<JIPclServiceCallback_Stub_ProxyClass, JIPclServiceCallback_Stub_Proxy>) end;

  JNetStatClass = interface(JObjectClass)
    ['{3454BC96-9E85-4108-ABDE-26C2B2377F9D}']
    {class} function init: JNetStat; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/NetStat')]
  JNetStat = interface(JObject)
    ['{5076833C-FF12-4D7F-806D-04B43FFFE2AC}']
    function getConnections: JArrayList; cdecl;
  end;
  TJNetStat = class(TJavaGenericImport<JNetStatClass, JNetStat>) end;

  JNetStat_ConnectionClass = interface(JObjectClass)
    ['{CB227F0D-F27E-4D38-9341-E54AFC2E8847}']
    {class} function init(P1: JNetStat): JNetStat_Connection; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/NetStat$Connection')]
  JNetStat_Connection = interface(JObject)
    ['{BB6FDC24-B680-4931-8438-8A60155413D3}']
  end;
  TJNetStat_Connection = class(TJavaGenericImport<JNetStat_ConnectionClass, JNetStat_Connection>) end;

  JPclServiceClass = interface(JServiceClass)
    ['{C56B11E1-C3F4-4EDD-B8DA-0EB61FDCD444}']
    {class} function init: JPclService; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/PclService')]
  JPclService = interface(JService)
    ['{B67F1E4E-79A3-4C72-B76B-E852C4913DAE}']
    function AddDynamicBridge(P1: Integer; P2: Integer): Integer; cdecl;
    function AddDynamicBridgeLocal(P1: Integer; P2: Integer): Integer; cdecl;
    procedure StartPCLFromJNI; cdecl;
    procedure StopPCLFromJNI; cdecl;
//    function addDynamicBridge(P1: Integer; P2: Integer): Integer; cdecl;
//    function addDynamicBridgeLocal(P1: Integer; P2: Integer): Integer; cdecl;
    procedure barcodeEvent(P1: TJavaArray<Byte>); cdecl;
    procedure barcodeEventClose; cdecl;
    procedure barcodeEventExt(P1: TJavaArray<Byte>; P2: TJavaArray<Byte>); cdecl;
    function bcrDisableSymbologies(P1: TJavaArray<Integer>; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function bcrEnableSymbologies(P1: TJavaArray<Integer>; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function bcrEnableTrigger(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrGetFirmwareVersion(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrGetSettingsVersion(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetBeep(P1: Integer; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetGoodScanBeep(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetIlluminationMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetImagerMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetLightingMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetNonVolatileMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetReaderMode(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSetSettingsVersion(P1: TJavaArray<Byte>; P2: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSoftReset(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrStartScan(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrStopScan(P1: TJavaArray<Byte>): Boolean; cdecl;
    function bcrSymbologyToText(P1: Integer): JString; cdecl;
    function closeBarcode(P1: TJavaArray<Byte>): Boolean; cdecl;
    function closePrinter(P1: TJavaArray<Byte>): Boolean; cdecl;
    function doTransaction(P1: JTransactionIn; P2: JTransactionOut): Boolean; cdecl;
    function doTransactionEx(P1: JTransactionIn; P2: JTransactionOut; P3: Integer; P4: TJavaArray<Byte>; P5: Int64; P6: TJavaArray<Byte>; P7: TJavaArray<Int64>): Boolean; cdecl;
    function doUpdate(P1: TJavaArray<Byte>): Boolean; cdecl;
    function flushMessages: Boolean; cdecl;
    function getBatteryInfo: TJavaArray<Integer>; cdecl;
    function getConnectionStatus(P1: Integer): TJavaArray<Byte>; cdecl;
    function getFullSerialNumber(P1: TJavaArray<Byte>): Boolean; cdecl;
    function getPrinterStatus(P1: TJavaArray<Byte>): Boolean; cdecl;
    function getSPMCIVersion(P1: TJavaArray<Byte>): Boolean; cdecl;
    function getSerialNumber: JString; cdecl;
    function getSignatureCapture(P1: Integer; P2: Integer; P3: Integer; P4: Integer; P5: Integer): Integer; cdecl;
    function getSignatureCaptureBitmap: TJavaArray<Byte>; cdecl;
    function getSignatureCaptureResult: Integer; cdecl;
    function getSimCardNumber: TJavaArray<Byte>; cdecl;
    function getTerminalComponents(P1: JString): Boolean; cdecl;
    function getTerminalInfo(P1: TJavaArray<Byte>; P2: TJavaArray<Byte>): Boolean; cdecl;
    function getTerminalTime(P1: TJavaArray<Byte>): Boolean; cdecl;
    function inputSimul(P1: TJavaArray<Byte>): Boolean; cdecl;
    function isPrinterBatteryLow(P1: TJavaArray<Byte>): Boolean; cdecl;
    function isPrinterConnected(P1: TJavaArray<Byte>): Boolean; cdecl;
    function launchM2OSShortcut(P1: TJavaArray<Byte>): Boolean; cdecl;
    procedure notifyConnection; cdecl;
    procedure notifyDisconnection; cdecl;
    function onBind(P1: JIntent): JIBinder; cdecl;
    procedure onCreate; cdecl;
    procedure onDestroy; cdecl;
    function onStartCommand(P1: JIntent; P2: Integer; P3: Integer): Integer; cdecl;
    function openBarcode(P1: TJavaArray<Byte>): Boolean; cdecl;
    function openBarcodeWithInactivityTo(P1: Integer; P2: TJavaArray<Byte>): Boolean; cdecl;
    function openPrinter(P1: TJavaArray<Byte>): Boolean; cdecl;
    function printBitmap(P1: TJavaArray<Byte>; P2: Integer; P3: TJavaArray<Byte>): Boolean; cdecl;
    function printBitmapObject(P1: JBitmap; P2: TJavaArray<Byte>): Boolean; cdecl;
    function printLogo(P1: JString; P2: TJavaArray<Byte>): Boolean; cdecl;
    function printText(P1: JString; P2: TJavaArray<Byte>): Boolean; cdecl;
    function receiveMessage(P1: TJavaArray<Byte>; P2: TJavaArray<Integer>): Boolean; cdecl;
    procedure registerCallback(P1: JIPclServiceCallback); cdecl;
    function resetTerminal(P1: Integer): Boolean; cdecl;
    function sendMessage(P1: TJavaArray<Byte>; P2: TJavaArray<Integer>): Boolean; cdecl;
    function serverStatus(P1: TJavaArray<Byte>): Boolean; cdecl;
    procedure setSignatureCaptureResult(P1: Integer); cdecl;
    function setTerminalTime(P1: TJavaArray<Byte>): Boolean; cdecl;
    function shouldAddSignature: Integer; cdecl;
    function shouldCutPaper: Integer; cdecl;
    function shouldEndReceipt: Integer; cdecl;
    function shouldFeedPaper(P1: Integer): Integer; cdecl;
    function shouldPrintImage(P1: Integer; P2: Integer; P3: TJavaArray<Byte>; P4: Byte): Integer; cdecl;
    function shouldPrintText(P1: TJavaArray<Byte>; P2: Byte; P3: Byte; P4: Byte; P5: Byte; P6: Byte): Integer; cdecl;
    function shouldStartReceipt(P1: Byte): Integer; cdecl;
    function storeLogo(P1: JString; P2: Integer; P3: TJavaArray<Byte>; P4: Integer; P5: TJavaArray<Byte>): Boolean; cdecl;
    procedure submitSignatureWithImage(P1: JBitmap); cdecl;
    function tmsReadParam(P1: TJavaObjectArray<JString>; P2: TJavaObjectArray<JString>; P3: TJavaObjectArray<JString>; P4: TJavaObjectArray<JString>; P5: TJavaObjectArray<JString>; P6: TJavaArray<Byte>): Boolean; cdecl;
    function tmsWriteParam(P1: JString; P2: JString; P3: JString; P4: JString; P5: TJavaArray<Byte>): Boolean; cdecl;
    procedure unregisterCallback(P1: JIPclServiceCallback); cdecl;
  end;
  TJPclService = class(TJavaGenericImport<JPclServiceClass, JPclService>) end;

  JPclService_1Class = interface(JHandlerClass)
    ['{EDE0B3F2-EC84-413B-9AEF-D75AE383AFFE}']
  end;

  [JavaSignature('com/ingenico/pclservice/PclService$1')]
  JPclService_1 = interface(JHandler)
    ['{57568DD2-4753-41AE-B875-F8EE29CA11BB}']
    procedure handleMessage(P1: JMessage); cdecl;
  end;
  TJPclService_1 = class(TJavaGenericImport<JPclService_1Class, JPclService_1>) end;

  JPclService_2Class = interface(JBroadcastReceiverClass)
    ['{87351F57-6B13-4A65-9516-59060D6DBF7D}']
  end;

  [JavaSignature('com/ingenico/pclservice/PclService$2')]
  JPclService_2 = interface(JBroadcastReceiver)
    ['{CF2FF1A0-12D2-4F99-A5EC-C6C4DEBFB687}']
    procedure onReceive(P1: JContext; P2: JIntent); cdecl;
  end;
  TJPclService_2 = class(TJavaGenericImport<JPclService_2Class, JPclService_2>) end;

  JTimerTaskClass = interface(JObjectClass)
    ['{DC15DA86-BDCC-42A9-8B9D-7348D4AE0F13}']
  end;

  [JavaSignature('java/util/TimerTask')]
  JTimerTask = interface(JObject)
    ['{B01AA454-6E9B-4A26-A31E-8D9A32E59816}']
    function cancel: Boolean; cdecl;
    procedure run; cdecl;
    function scheduledExecutionTime: Int64; cdecl;
  end;
  TJTimerTask = class(TJavaGenericImport<JTimerTaskClass, JTimerTask>) end;

  JPclService_3Class = interface(JTimerTaskClass)
    ['{1F00CD05-4E6D-4D4A-B8C5-E4A4C17D36AF}']
  end;

  [JavaSignature('com/ingenico/pclservice/PclService$3')]
  JPclService_3 = interface(JTimerTask)
    ['{84483A0A-68D7-4CBB-88CE-22BF0C4CE8BD}']
    procedure run; cdecl;
  end;
  TJPclService_3 = class(TJavaGenericImport<JPclService_3Class, JPclService_3>) end;

  JPclService_LocalBinderClass = interface(JBinderClass)
    ['{C68EAD4C-4B95-44B9-B2C9-D3F6CF88710A}']
    {class} function init(P1: JPclService): JPclService_LocalBinder; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/PclService$LocalBinder')]
  JPclService_LocalBinder = interface(JBinder)
    ['{2F072A8E-7922-4FDA-B0EE-E94C7FFD708A}']
    function getService: JPclService; cdecl;
  end;
  TJPclService_LocalBinder = class(TJavaGenericImport<JPclService_LocalBinderClass, JPclService_LocalBinder>) end;

  JPclService_SignObjectClass = interface(JObjectClass)
    ['{375D691D-70FF-4F98-BDBA-8988894D94E6}']
    {class} function init(P1: JPclService; P2: Integer; P3: Integer; P4: Integer; P5: Integer; P6: Integer): JPclService_SignObject; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/PclService$SignObject')]
  JPclService_SignObject = interface(JObject)
    ['{9022308A-EA92-42DE-A324-CFC9401D2AD0}']
    function getHeight: Integer; cdecl;
    function getTimeout: Integer; cdecl;
    function getWidth: Integer; cdecl;
    function getXpos: Integer; cdecl;
    function getYpos: Integer; cdecl;
    procedure setHeight(P1: Integer); cdecl;
    procedure setTimeout(P1: Integer); cdecl;
    procedure setWidth(P1: Integer); cdecl;
    procedure setXpos(P1: Integer); cdecl;
    procedure setYpos(P1: Integer); cdecl;
  end;
  TJPclService_SignObject = class(TJavaGenericImport<JPclService_SignObjectClass, JPclService_SignObject>) end;

  JPclService_SymbologiesClass = interface(JEnumClass)
    ['{5B471900-3A0A-4E55-89CF-33AAA19C74A7}']
    {class} function _GetICBarCode_11: JPclService_Symbologies;
    {class} function _GetICBarCode_93: JPclService_Symbologies;
    {class} function _GetICBarCode_AllSymbologies: JPclService_Symbologies;
    {class} function _GetICBarCode_AmesCode: JPclService_Symbologies;
    {class} function _GetICBarCode_Aztec: JPclService_Symbologies;
    {class} function _GetICBarCode_CodaBar: JPclService_Symbologies;
    {class} function _GetICBarCode_CodaBlockA: JPclService_Symbologies;
    {class} function _GetICBarCode_CodaBlockF: JPclService_Symbologies;
    {class} function _GetICBarCode_Code128: JPclService_Symbologies;
    {class} function _GetICBarCode_Code16K: JPclService_Symbologies;
    {class} function _GetICBarCode_Code39: JPclService_Symbologies;
    {class} function _GetICBarCode_Code39_ItalianCPI: JPclService_Symbologies;
    {class} function _GetICBarCode_Code49: JPclService_Symbologies;
    {class} function _GetICBarCode_DataMatrix: JPclService_Symbologies;
    {class} function _GetICBarCode_EAN13: JPclService_Symbologies;
    {class} function _GetICBarCode_EAN13_2: JPclService_Symbologies;
    {class} function _GetICBarCode_EAN13_5: JPclService_Symbologies;
    {class} function _GetICBarCode_EAN8: JPclService_Symbologies;
    {class} function _GetICBarCode_EAN8_2: JPclService_Symbologies;
    {class} function _GetICBarCode_EAN8_5: JPclService_Symbologies;
    {class} function _GetICBarCode_GS1_128: JPclService_Symbologies;
    {class} function _GetICBarCode_GS1_DataBarExpanded: JPclService_Symbologies;
    {class} function _GetICBarCode_GS1_DataBarLimited: JPclService_Symbologies;
    {class} function _GetICBarCode_GS1_DataBarOmni: JPclService_Symbologies;
    {class} function _GetICBarCode_ISBT128: JPclService_Symbologies;
    {class} function _GetICBarCode_Interleaved2of5: JPclService_Symbologies;
    {class} function _GetICBarCode_MSI: JPclService_Symbologies;
    {class} function _GetICBarCode_Matrix2of5: JPclService_Symbologies;
    {class} function _GetICBarCode_MaxIndex: JPclService_Symbologies;
    {class} function _GetICBarCode_Maxicode: JPclService_Symbologies;
    {class} function _GetICBarCode_MicroPDF: JPclService_Symbologies;
    {class} function _GetICBarCode_PDF417: JPclService_Symbologies;
    {class} function _GetICBarCode_Plessey: JPclService_Symbologies;
    {class} function _GetICBarCode_QRCode: JPclService_Symbologies;
    {class} function _GetICBarCode_Standard2of5: JPclService_Symbologies;
    {class} function _GetICBarCode_Telepen: JPclService_Symbologies;
    {class} function _GetICBarCode_UPCA: JPclService_Symbologies;
    {class} function _GetICBarCode_UPCA_2: JPclService_Symbologies;
    {class} function _GetICBarCode_UPCA_5: JPclService_Symbologies;
    {class} function _GetICBarCode_UPCE: JPclService_Symbologies;
    {class} function _GetICBarCode_UPCE1: JPclService_Symbologies;
    {class} function _GetICBarCode_UPCE_2: JPclService_Symbologies;
    {class} function _GetICBarCode_UPCE_5: JPclService_Symbologies;
    {class} function _GetICBarCode_Unknown: JPclService_Symbologies;
    {class} function valueOf(P1: JString): JPclService_Symbologies; cdecl;
    {class} function values: TJavaObjectArray<JPclService_Symbologies>; cdecl;
    {class} property ICBarCode_11: JPclService_Symbologies read _GetICBarCode_11;
    {class} property ICBarCode_93: JPclService_Symbologies read _GetICBarCode_93;
    {class} property ICBarCode_AllSymbologies: JPclService_Symbologies read _GetICBarCode_AllSymbologies;
    {class} property ICBarCode_AmesCode: JPclService_Symbologies read _GetICBarCode_AmesCode;
    {class} property ICBarCode_Aztec: JPclService_Symbologies read _GetICBarCode_Aztec;
    {class} property ICBarCode_CodaBar: JPclService_Symbologies read _GetICBarCode_CodaBar;
    {class} property ICBarCode_CodaBlockA: JPclService_Symbologies read _GetICBarCode_CodaBlockA;
    {class} property ICBarCode_CodaBlockF: JPclService_Symbologies read _GetICBarCode_CodaBlockF;
    {class} property ICBarCode_Code128: JPclService_Symbologies read _GetICBarCode_Code128;
    {class} property ICBarCode_Code16K: JPclService_Symbologies read _GetICBarCode_Code16K;
    {class} property ICBarCode_Code39: JPclService_Symbologies read _GetICBarCode_Code39;
    {class} property ICBarCode_Code39_ItalianCPI: JPclService_Symbologies read _GetICBarCode_Code39_ItalianCPI;
    {class} property ICBarCode_Code49: JPclService_Symbologies read _GetICBarCode_Code49;
    {class} property ICBarCode_DataMatrix: JPclService_Symbologies read _GetICBarCode_DataMatrix;
    {class} property ICBarCode_EAN13: JPclService_Symbologies read _GetICBarCode_EAN13;
    {class} property ICBarCode_EAN13_2: JPclService_Symbologies read _GetICBarCode_EAN13_2;
    {class} property ICBarCode_EAN13_5: JPclService_Symbologies read _GetICBarCode_EAN13_5;
    {class} property ICBarCode_EAN8: JPclService_Symbologies read _GetICBarCode_EAN8;
    {class} property ICBarCode_EAN8_2: JPclService_Symbologies read _GetICBarCode_EAN8_2;
    {class} property ICBarCode_EAN8_5: JPclService_Symbologies read _GetICBarCode_EAN8_5;
    {class} property ICBarCode_GS1_128: JPclService_Symbologies read _GetICBarCode_GS1_128;
    {class} property ICBarCode_GS1_DataBarExpanded: JPclService_Symbologies read _GetICBarCode_GS1_DataBarExpanded;
    {class} property ICBarCode_GS1_DataBarLimited: JPclService_Symbologies read _GetICBarCode_GS1_DataBarLimited;
    {class} property ICBarCode_GS1_DataBarOmni: JPclService_Symbologies read _GetICBarCode_GS1_DataBarOmni;
    {class} property ICBarCode_ISBT128: JPclService_Symbologies read _GetICBarCode_ISBT128;
    {class} property ICBarCode_Interleaved2of5: JPclService_Symbologies read _GetICBarCode_Interleaved2of5;
    {class} property ICBarCode_MSI: JPclService_Symbologies read _GetICBarCode_MSI;
    {class} property ICBarCode_Matrix2of5: JPclService_Symbologies read _GetICBarCode_Matrix2of5;
    {class} property ICBarCode_MaxIndex: JPclService_Symbologies read _GetICBarCode_MaxIndex;
    {class} property ICBarCode_Maxicode: JPclService_Symbologies read _GetICBarCode_Maxicode;
    {class} property ICBarCode_MicroPDF: JPclService_Symbologies read _GetICBarCode_MicroPDF;
    {class} property ICBarCode_PDF417: JPclService_Symbologies read _GetICBarCode_PDF417;
    {class} property ICBarCode_Plessey: JPclService_Symbologies read _GetICBarCode_Plessey;
    {class} property ICBarCode_QRCode: JPclService_Symbologies read _GetICBarCode_QRCode;
    {class} property ICBarCode_Standard2of5: JPclService_Symbologies read _GetICBarCode_Standard2of5;
    {class} property ICBarCode_Telepen: JPclService_Symbologies read _GetICBarCode_Telepen;
    {class} property ICBarCode_UPCA: JPclService_Symbologies read _GetICBarCode_UPCA;
    {class} property ICBarCode_UPCA_2: JPclService_Symbologies read _GetICBarCode_UPCA_2;
    {class} property ICBarCode_UPCA_5: JPclService_Symbologies read _GetICBarCode_UPCA_5;
    {class} property ICBarCode_UPCE: JPclService_Symbologies read _GetICBarCode_UPCE;
    {class} property ICBarCode_UPCE1: JPclService_Symbologies read _GetICBarCode_UPCE1;
    {class} property ICBarCode_UPCE_2: JPclService_Symbologies read _GetICBarCode_UPCE_2;
    {class} property ICBarCode_UPCE_5: JPclService_Symbologies read _GetICBarCode_UPCE_5;
    {class} property ICBarCode_Unknown: JPclService_Symbologies read _GetICBarCode_Unknown;
  end;

  [JavaSignature('com/ingenico/pclservice/PclService$Symbologies')]
  JPclService_Symbologies = interface(JEnum)
    ['{D48F8F5A-B02B-41FA-8389-99B0AEAE8DE3}']
    function toString: JString; cdecl;
  end;
  TJPclService_Symbologies = class(TJavaGenericImport<JPclService_SymbologiesClass, JPclService_Symbologies>) end;

  JPclService_TextObjectClass = interface(JObjectClass)
    ['{4EEAEE7E-B564-4739-8B8E-DD38A27FDAB7}']
    {class} function init(P1: JPclService; P2: TJavaArray<Byte>; P3: Byte; P4: Byte; P5: Byte; P6: Byte; P7: Byte): JPclService_TextObject; cdecl;
  end;

  [JavaSignature('com/ingenico/pclservice/PclService$TextObject')]
  JPclService_TextObject = interface(JObject)
    ['{E3F1F273-79DA-406C-A248-09820BE5B43F}']
    function getBold: Byte; cdecl;
    function getCharset: Byte; cdecl;
    function getFont: Byte; cdecl;
    function getJustification: Byte; cdecl;
    function getMode: Byte; cdecl;
    function getText: TJavaArray<Byte>; cdecl;
    procedure setBold(P1: Byte); cdecl;
    procedure setCharset(P1: Byte); cdecl;
    procedure setFont(P1: Byte); cdecl;
    procedure setJustification(P1: Byte); cdecl;
    procedure setMode(P1: Byte); cdecl;
    procedure setText(P1: TJavaArray<Byte>); cdecl;
  end;
  TJPclService_TextObject = class(TJavaGenericImport<JPclService_TextObjectClass, JPclService_TextObject>) end;

  JTransactionInClass = interface(JObjectClass)
    ['{7866F9FD-9799-4400-83D5-9CCCC2CB4280}']
    {class} function _GetCREATOR: JParcelable_Creator;
    {class} function init: JTransactionIn; cdecl; overload;
    {class} property CREATOR: JParcelable_Creator read _GetCREATOR;
  end;

  [JavaSignature('com/ingenico/pclservice/TransactionIn')]
  JTransactionIn = interface(JObject)
    ['{C2DA39FB-AEDF-42B6-B072-5DAFCF4EDEC0}']
    function describeContents: Integer; cdecl;
    function getAmount: JString; cdecl;
    function getAuthorizationType: JString; cdecl;
    function getCtrlCheque: JString; cdecl;
    function getCurrencyCode: JString; cdecl;
    function getLength: Integer; cdecl;
    function getOperation: JString; cdecl;
    function getTermNum: JString; cdecl;
    function getUserData1: JString; cdecl;
    procedure readFromParcel(P1: JParcel); cdecl;
    function setAmount(P1: JString): Boolean; cdecl;
    function setAuthorizationType(P1: JString): Boolean; cdecl;
    function setCtrlCheque(P1: JString): Boolean; cdecl;
    function setCurrencyCode(P1: JString): Boolean; cdecl;
    function setOperation(P1: JString): Boolean; cdecl;
    function setTermNum(P1: JString): Boolean; cdecl;
    function setUserData1(P1: JString): Boolean; cdecl;
    procedure writeToParcel(P1: JParcel; P2: Integer); cdecl;
  end;
  TJTransactionIn = class(TJavaGenericImport<JTransactionInClass, JTransactionIn>) end;

  JTransactionIn_1Class = interface(JObjectClass)
    ['{53747CA0-9229-497F-9367-27179A9C4E99}']
  end;

  [JavaSignature('com/ingenico/pclservice/TransactionIn$1')]
  JTransactionIn_1 = interface(JObject)
    ['{C09FA284-EFD6-4A9D-9959-70CFBF6635FC}']
    function createFromParcel(P1: JParcel): JTransactionIn; cdecl;
    function newArray(P1: Integer): TJavaObjectArray<JTransactionIn>; cdecl;
  end;
  TJTransactionIn_1 = class(TJavaGenericImport<JTransactionIn_1Class, JTransactionIn_1>) end;

  JTransactionIn_AuthorizationTypeClass = interface(JEnumClass)
    ['{7B3E5F9D-6B68-4654-9067-4EA1A2E3EB8C}']
    {class} function _GetAUTHORIZATION_FORCED: JTransactionIn_AuthorizationType;
    {class} function _GetAUTHORIZATION_NOT_FORCED: JTransactionIn_AuthorizationType;
    {class} function valueOf(P1: JString): JTransactionIn_AuthorizationType; cdecl;
    {class} function values: TJavaObjectArray<JTransactionIn_AuthorizationType>; cdecl;
    {class} property AUTHORIZATION_FORCED: JTransactionIn_AuthorizationType read _GetAUTHORIZATION_FORCED;
    {class} property AUTHORIZATION_NOT_FORCED: JTransactionIn_AuthorizationType read _GetAUTHORIZATION_NOT_FORCED;
  end;

  [JavaSignature('com/ingenico/pclservice/TransactionIn$AuthorizationType')]
  JTransactionIn_AuthorizationType = interface(JEnum)
    ['{A09E4A71-98E6-4DFA-97A9-309D0712BA5F}']
    function toString: JString; cdecl;
  end;
  TJTransactionIn_AuthorizationType = class(TJavaGenericImport<JTransactionIn_AuthorizationTypeClass, JTransactionIn_AuthorizationType>) end;

  JTransactionIn_CheckControlClass = interface(JEnumClass)
    ['{A99EF0B5-A72D-42D5-B9BE-06FB2097E88A}']
    {class} function _GetCHECK_CONTROL_FNCI: JTransactionIn_CheckControl;
    {class} function _GetCHECK_CONTROL_GUARANTEE: JTransactionIn_CheckControl;
    {class} function valueOf(P1: JString): JTransactionIn_CheckControl; cdecl;
    {class} function values: TJavaObjectArray<JTransactionIn_CheckControl>; cdecl;
    {class} property CHECK_CONTROL_FNCI: JTransactionIn_CheckControl read _GetCHECK_CONTROL_FNCI;
    {class} property CHECK_CONTROL_GUARANTEE: JTransactionIn_CheckControl read _GetCHECK_CONTROL_GUARANTEE;
  end;

  [JavaSignature('com/ingenico/pclservice/TransactionIn$CheckControl')]
  JTransactionIn_CheckControl = interface(JEnum)
    ['{B995F517-9002-4185-834F-95C913F0D6A2}']
    function toString: JString; cdecl;
  end;
  TJTransactionIn_CheckControl = class(TJavaGenericImport<JTransactionIn_CheckControlClass, JTransactionIn_CheckControl>) end;

  JTransactionIn_OperationTypeClass = interface(JEnumClass)
    ['{F36EDDD3-6078-45DB-9755-EFC8FC1A2458}']
    {class} function _GetOPERATION_CANCEL: JTransactionIn_OperationType;
    {class} function _GetOPERATION_CREDIT: JTransactionIn_OperationType;
    {class} function _GetOPERATION_DEBIT: JTransactionIn_OperationType;
    {class} function _GetOPERATION_DUPLICATE: JTransactionIn_OperationType;
    {class} function valueOf(P1: JString): JTransactionIn_OperationType; cdecl;
    {class} function values: TJavaObjectArray<JTransactionIn_OperationType>; cdecl;
    {class} property OPERATION_CANCEL: JTransactionIn_OperationType read _GetOPERATION_CANCEL;
    {class} property OPERATION_CREDIT: JTransactionIn_OperationType read _GetOPERATION_CREDIT;
    {class} property OPERATION_DEBIT: JTransactionIn_OperationType read _GetOPERATION_DEBIT;
    {class} property OPERATION_DUPLICATE: JTransactionIn_OperationType read _GetOPERATION_DUPLICATE;
  end;

  [JavaSignature('com/ingenico/pclservice/TransactionIn$OperationType')]
  JTransactionIn_OperationType = interface(JEnum)
    ['{ABA21B76-5A67-4F05-8385-C801CDCDD184}']
    function toString: JString; cdecl;
  end;
  TJTransactionIn_OperationType = class(TJavaGenericImport<JTransactionIn_OperationTypeClass, JTransactionIn_OperationType>) end;

  JTransactionOutClass = interface(JObjectClass)
    ['{F5DF381A-566A-4AE9-A0EE-8C2D40824DF1}']
    {class} function _GetCREATOR: JParcelable_Creator;
    {class} function init: JTransactionOut; cdecl; overload;
    {class} property CREATOR: JParcelable_Creator read _GetCREATOR;
  end;

  [JavaSignature('com/ingenico/pclservice/TransactionOut')]
  JTransactionOut = interface(JObject)
    ['{3B4D9A0F-DF1C-4C8F-8E57-3F561555354C}']
    function describeContents: Integer; cdecl;
    function getAmount: JString; cdecl;
    function getC3Error: JString; cdecl;
    function getCurrencyCode: JString; cdecl;
    function getFFU: JString; cdecl;
    function getLength: Integer; cdecl;
    function getTerminalNumber: JString; cdecl;
    function getUserData: JString; cdecl;
    procedure readFromParcel(P1: JParcel); cdecl;
    procedure writeToParcel(P1: JParcel; P2: Integer); cdecl;
  end;
  TJTransactionOut = class(TJavaGenericImport<JTransactionOutClass, JTransactionOut>) end;

  JTransactionOut_1Class = interface(JObjectClass)
    ['{6C17049F-DD1D-464B-901A-CF5C270A4C08}']
  end;

  [JavaSignature('com/ingenico/pclservice/TransactionOut$1')]
  JTransactionOut_1 = interface(JObject)
    ['{A5A3120C-FB87-4811-9BCF-9226188C48CA}']
    function createFromParcel(P1: JParcel): JTransactionOut; cdecl;
    function newArray(P1: Integer): TJavaObjectArray<JTransactionOut>; cdecl;
  end;
  TJTransactionOut_1 = class(TJavaGenericImport<JTransactionOut_1Class, JTransactionOut_1>) end;

  JTransactionOut_ErrorCodeClass = interface(JEnumClass)
    ['{A14C706E-0E04-4457-B379-6F16540BB2B8}']
    {class} function _GetERROR: JTransactionOut_ErrorCode;
    {class} function _GetSUCCESS: JTransactionOut_ErrorCode;
    {class} function valueOf(P1: JString): JTransactionOut_ErrorCode; cdecl;
    {class} function values: TJavaObjectArray<JTransactionOut_ErrorCode>; cdecl;
    {class} property ERROR: JTransactionOut_ErrorCode read _GetERROR;
    {class} property SUCCESS: JTransactionOut_ErrorCode read _GetSUCCESS;
  end;

  [JavaSignature('com/ingenico/pclservice/TransactionOut$ErrorCode')]
  JTransactionOut_ErrorCode = interface(JEnum)
    ['{859B41FE-1031-4539-A4CE-ED7685C04E76}']
    function toString: JString; cdecl;
  end;
  TJTransactionOut_ErrorCode = class(TJavaGenericImport<JTransactionOut_ErrorCodeClass, JTransactionOut_ErrorCode>) end;

implementation

procedure RegisterTypes;
begin
  TRegTypes.RegisterType('Android.JNI.Ingenico.JBitmapConvertor', TypeInfo(Android.JNI.Ingenico.JBitmapConvertor));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JBluetoothService', TypeInfo(Android.JNI.Ingenico.JBluetoothService));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JBluetoothService_1', TypeInfo(Android.JNI.Ingenico.JBluetoothService_1));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JBluetoothService_2', TypeInfo(Android.JNI.Ingenico.JBluetoothService_2));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JBluetoothService_BluetoothThread', TypeInfo(Android.JNI.Ingenico.JBluetoothService_BluetoothThread));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JBluetoothService_IpThread', TypeInfo(Android.JNI.Ingenico.JBluetoothService_IpThread));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JBluetoothService_IpTxThread', TypeInfo(Android.JNI.Ingenico.JBluetoothService_IpTxThread));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JBluetoothService_Status', TypeInfo(Android.JNI.Ingenico.JBluetoothService_Status));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JIPclService', TypeInfo(Android.JNI.Ingenico.JIPclService));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JIPclService_Stub', TypeInfo(Android.JNI.Ingenico.JIPclService_Stub));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JIPclService_Stub_Proxy', TypeInfo(Android.JNI.Ingenico.JIPclService_Stub_Proxy));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JIPclServiceCallback', TypeInfo(Android.JNI.Ingenico.JIPclServiceCallback));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JIPclServiceCallback_Stub', TypeInfo(Android.JNI.Ingenico.JIPclServiceCallback_Stub));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JIPclServiceCallback_Stub_Proxy', TypeInfo(Android.JNI.Ingenico.JIPclServiceCallback_Stub_Proxy));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JNetStat', TypeInfo(Android.JNI.Ingenico.JNetStat));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JNetStat_Connection', TypeInfo(Android.JNI.Ingenico.JNetStat_Connection));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JPclService', TypeInfo(Android.JNI.Ingenico.JPclService));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JPclService_1', TypeInfo(Android.JNI.Ingenico.JPclService_1));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JPclService_2', TypeInfo(Android.JNI.Ingenico.JPclService_2));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JTimerTask', TypeInfo(Android.JNI.Ingenico.JTimerTask));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JPclService_3', TypeInfo(Android.JNI.Ingenico.JPclService_3));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JPclService_LocalBinder', TypeInfo(Android.JNI.Ingenico.JPclService_LocalBinder));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JPclService_SignObject', TypeInfo(Android.JNI.Ingenico.JPclService_SignObject));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JPclService_Symbologies', TypeInfo(Android.JNI.Ingenico.JPclService_Symbologies));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JPclService_TextObject', TypeInfo(Android.JNI.Ingenico.JPclService_TextObject));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JTransactionIn', TypeInfo(Android.JNI.Ingenico.JTransactionIn));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JTransactionIn_1', TypeInfo(Android.JNI.Ingenico.JTransactionIn_1));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JTransactionIn_AuthorizationType', TypeInfo(Android.JNI.Ingenico.JTransactionIn_AuthorizationType));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JTransactionIn_CheckControl', TypeInfo(Android.JNI.Ingenico.JTransactionIn_CheckControl));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JTransactionIn_OperationType', TypeInfo(Android.JNI.Ingenico.JTransactionIn_OperationType));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JTransactionOut', TypeInfo(Android.JNI.Ingenico.JTransactionOut));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JTransactionOut_1', TypeInfo(Android.JNI.Ingenico.JTransactionOut_1));
  TRegTypes.RegisterType('Android.JNI.Ingenico.JTransactionOut_ErrorCode', TypeInfo(Android.JNI.Ingenico.JTransactionOut_ErrorCode));
end;

initialization
  RegisterTypes;
end.


