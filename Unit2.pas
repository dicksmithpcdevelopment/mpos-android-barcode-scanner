unit Unit2;

interface

uses System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.StdCtrls, FMX.Controls.Presentation,
   System.Actions, FMX.ActnList, FMX.Layouts, FMX.ListBox,
   ISMPScanner, Android.Scanner;

type


  TForm2 = class(TForm)
    btnLoadService: TButton;
    btnOpenScanner: TButton;
    Label1: TLabel;
    btnCloseScanner: TButton;
    Label2: TLabel;
    btnStopService: TButton;
    btnStartScanner: TButton;
    btnStopScanner: TButton;
    ActionList1: TActionList;
    ListBox1: TListBox;
    procedure btnLoadServiceClick(Sender: TObject);
    procedure btnOpenScannerClick(Sender: TObject);
    procedure btnCloseScannerClick(Sender: TObject);
    procedure btnStopServiceClick(Sender: TObject);
    procedure btnStartScannerClick(Sender: TObject);
    procedure btnStopScannerClick(Sender: TObject);
  private
    bcScanner: TISMPScan;
    procedure BarcodeCallback(const s: string);
  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses
  System.TypInfo;

{$R *.fmx}

procedure TForm2.btnOpenScannerClick(Sender: TObject);
begin
  Label1.text := '';

  if bcScanner.Active then
  begin
    if bcScanner.ScannerOpen then
     Label1.text := 'Scanner Open';
  end
  else
    Label1.text := 'Service is not running';
end;

procedure TForm2.BarcodeCallback(const s: string);
var
  i: integer;
  str: string;
begin
  str := '';
  for i := 0 to Length(s) do
    if ord(s[i]) >= 32 then
      str := str + s[i];
  ListBox1.Items.insert(0, inttostr(ListBox1.count) + ' ' + str);
end;

procedure TForm2.btnCloseScannerClick(Sender: TObject);
begin
  Label2.text := '';
  if bcScanner.Active then
  begin
    bcScanner.ScannerClose;
    Label2.text := 'Scanner Closed';
  end
  else
    Label2.text := 'Service is not running';
end;

procedure TForm2.btnLoadServiceClick(Sender: TObject);
begin
  if bcScanner = nil then
    bcScanner := TAndroidScan.create;
  bcScanner.Init;
end;

procedure TForm2.btnStopScannerClick(Sender: TObject);
//var
//  aResult: TJavaArray<byte>;
//  i: integer;
//  strResult: String;
begin
//  aResult := TJavaArray<byte>.Create(1);
//  try
//    aResult[0] := 0;
//    if testaConn.service.bcrStopScan(aResult) then
//    Begin
//      strResult := 'StopScan True ';
//    end
//    else
//      strResult := 'Stopscan False';
//
//    for i := 0 to aResult.Length Do
//      strResult := strResult + char(aResult[i]);
//
//    Toast(strResult, TToastLength.ShortToast);
//  finally
//    aResult.Free;
//  end;
end;

procedure TForm2.btnStopServiceClick(Sender: TObject);
begin
  bcScanner.Close;
end;

procedure TForm2.btnStartScannerClick(Sender: TObject);
//var
//  aResult: TJavaArray<byte>;
//  i: integer;
//  strResult: String;
begin
//  aResult := TJavaArray<byte>.Create(1);
//  try
//    aResult[0] := 0;
//    if testaConn.service.bcrStartScan(aResult) then
//    Begin
//      strResult := 'StartScan True ';
//    end
//    else
//      strResult := 'Startscan False';
//
//    for i := 0 to aResult.Length Do
//      strResult := strResult + char(aResult[i]);
//
//    Toast(strResult, TToastLength.ShortToast);
//  finally
//    aResult.Free;
//  end;
end;

end.
